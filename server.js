const path = require("path");
const express = require("express");

const { PORT = 80 } = process.env;

const app = express();

app.get("/", (req, res) => {
  const homeFile = path.resolve("./home.html");
  res.sendFile(homeFile);
});

app.listen(PORT, () => {
  console.log(`Listening on PORT ${PORT}`);
});
